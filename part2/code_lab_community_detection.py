"""
Graph Mining - ALTEGRAD - Dec 2018
"""

import networkx as nx
import numpy as np
from scipy.sparse.linalg import eigs
from random import randint
from sklearn.cluster import KMeans
from networkx.algorithms.community import greedy_modularity_communities

path = "datasets/CA-HepTh.txt"

# Load the graph into an undirected NetworkX graph
graph = nx.read_edgelist(path, comments='#', delimiter='\t', nodetype=int, create_using=nx.Graph())

# Get giant connected component (GCC)
gcc = max(nx.connected_component_subgraphs(graph), key=len)


############## Question 1
# Implement and apply spectral clustering
def spectral_clustering(p_graph, k):
    laplacian = nx.normalized_laplacian_matrix(p_graph).astype(float)  # Normalized Laplacian

    # Calculate k smallest in magnitude eigenvalues and corresponding eigenvectors of laplacian
    # hint: use eigs function of scipy

    ##################
    # your code here #
    ##################

    eigval, eigvec = eigs(laplacian, k=k, which='SR')

    eigval = eigval.real  # Keep the real part
    eigvec = eigvec.real  # Keep the real part

    idx = eigval.argsort()  # Get indices of sorted eigenvalues
    eigvec = eigvec[:, idx]  # Sort eigenvectors according to eigenvalues

    # Perform k-means clustering (store in variable "membership" the clusters to which points belong)
    # hint: use KMeans class of scikit-learn

    ##################
    # your code here #
    ##################

    kmeans = KMeans(n_clusters=k).fit(eigvec)

    # Create a dictionary "clustering" where keys are nodes and values the clusters to which the nodes belong

    ##################
    # your code here #
    ##################

    list_nodes = list(p_graph.nodes)
    r_clustering = {list_nodes[i]: kmeans.labels_[i] for i in range(len(list_nodes))}

    return r_clustering


# Apply spectral clustering to the CA-HepTh dataset
clustering = spectral_clustering(p_graph=gcc, k=60)

# sanity check
assert gcc.number_of_nodes() == len(clustering)


############## Question 2
# Implement modularity and compute it for two clustering results

# Compute modularity value from graph G based on clustering
def modularity(graph, clustering):
    n_clusters = len(set(clustering.values()))
    modularity = 0  # Initialize total modularity value

    m = graph.number_of_edges()

    # Iterate over all clusters
    for i in range(n_clusters):
        # Get the nodes that belong to the i-th cluster
        node_list = [n for n, v in clustering.items() if v == i]

        subG = graph.subgraph(node_list)  # get subgraph that corresponds to current cluster

        # Compute contribution of current cluster to modularity as in equation 1

        ##################
        # your code here #
        ##################		

        lc = subG.number_of_edges()
        degree_sequence = [d for n, d in graph.degree() if n in node_list]

        dc = np.sum(degree_sequence)

        modularity += (lc / m) - (dc / (2 * m)) ** 2

    return modularity


print("Modularity Spectral Clustering: ", modularity(gcc, clustering))

# Implement random clustering
k = 60
r_clustering = dict()

# Partition randomly the nodes in k clusters (store the clustering result in the "r_clustering" dictionary)
# hint: use randint function

##################
# your code here #
##################

for n in gcc.nodes():
    r_clustering.update({n: randint(0, k - 1)})

print("Modularity Random Clustering: ", modularity(gcc, r_clustering))

############## Question 3
# Run Clauset-Newman-Moore algorithm and compute modularity

# Partition graph using the Clauset-Newman-Moore greedy algorithm
# hint: use the greedy_modularity_communities function. The function returns sets of nodes, one for each community.
# Create a dictionary "clustering_cnm" keyed by node to the cluster to which the node belongs

##################
# your code here #
##################

cnm = greedy_modularity_communities(gcc)

clustering_cnm = dict()
for i in range(len(cnm)):
    cluster = cnm[i]
    for node in cluster:
        clustering_cnm.update({node: i})

print("Modularity Clauset-Newman-Moore algorithm: ", modularity(gcc, clustering_cnm))

print("\nLogically, the CNM, as optimising its modularity, gets a better modularity")
