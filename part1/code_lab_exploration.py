"""
Graph Mining - ALTEGRAD - Dec 2018
"""

# Import modules
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt

############## Question 1
# Load the graph into an undirected NetworkX graph

##################
# your codehere #
##################

path = "datasets/CA-HepTh.txt"

graph = nx.read_edgelist(path, comments='#', delimiter='\t', create_using=nx.Graph(), nodetype=int, data=True, edgetype=int,
                         encoding='utf−8')

############## Question 2
# Network Characteristics

##################
# your code here #
##################

print("Number of nodes: " + str(graph.number_of_nodes()))
print("Number of edges: " + str(graph.number_of_edges()))
print("Number of connected components: " + str(len(sorted(nx.connected_component_subgraphs(graph), key=len))))

gcc = max(nx.connected_component_subgraphs(graph), key=len)

print("Number of nodes of GCC: " + str(gcc.number_of_nodes()))
print("Number of edges of GCC: " + str(gcc.number_of_edges()))
print("\nProportion of nodes of GCC: " + str(gcc.number_of_nodes() / graph.number_of_nodes()))
print("Number of edges of GCC: " + str(gcc.number_of_edges() / graph.number_of_edges()))

print("*****\nThey correspond to the main part of the graph\n*****\n")

############## Question 3
# Analysis of degree distribution


##################
# your code here #
##################

degree_sequence = [d for n, d in graph.degree()]

print("Max degree: " + str(max(degree_sequence)))
print("Min degree: " + str(min(degree_sequence)))
print("Mean degree: " + str(np.mean(degree_sequence)))

y = nx.degree_histogram(graph)
plt.subplot(121)
plt.plot(y, 'b-', marker='o')
plt.ylabel("Frequency")
plt.xlabel("Degree")

plt.subplot(122)
plt.loglog(y, 'b-', marker='o')
plt.ylabel("Log Frequency")
plt.xlabel("Log Degree")

plt.show()

print("We observe that the degree is 'log linear' to the frequence. Thus, it's of the form d=F^n")
